"use strict";

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    createdCarts:[
        {
            type: Schema.Types.ObjectId,
            ref: 'Cart'
        }
    ]    
})

module.exports = mongoose.model('User', userSchema);
 