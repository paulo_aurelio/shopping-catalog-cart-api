
# VR-BackendTest-v1.1 - Headless GraphQL Shopping Cart and Catalog API


## Requirements

I have used the NoSql MongoDB database for implementing the persintence layer for this API.

The other dependences will be automatic installed by ```npm install``` from repository folder, after ```git clone``` the source code.

### MondoDB database

I downloaded from this website:
* https://www.mongodb.com/download-center

I installed on my mac this version: **mongodb-osx-ssl-x86_64-4.0.9.tgz**

After downloading and extracting the MongoDB binaries in a folder (MONGO_HOME), it is necessary to create the ```data``` folder for database files.

```
cd MONGO_HOME
mkdir data

```

I have prepared this script bash **(startup-mongod.sh)** which has to stay in the MONGO_HOME folder for starting up MondoDB.

```
MONGO_HOME="$(pwd)"
 
echo "starting mongodb at $MONGO_HOME/data/"

$MONGO_HOME/bin/mongod --dbpath $MONGO_HOME/data/
```

**Keep MondoDB running (database test, port 27017 - default settings)**

```
improver@Paulos-MacBook-Pro:~/Projetos/jobs-tests/ezyVet/mongodb-osx-x86_64-4.0.9$ ./startup-mongod.sh

...
2019-05-07T22:09:19.216-0300 I CONTROL  [initandlisten] 
2019-05-07T22:09:19.246-0300 I FTDC     [initandlisten] Initializing full-time diagnostic data capture with directory '/Users/improver/Projetos/jobs-tests/ezyVet/mongodb-osx-x86_64-4.0.9/data/diagnostic.data'
2019-05-07T22:09:19.251-0300 I NETWORK  [initandlisten] waiting for connections on port 27017
```

### Downloading API Source Code git repository

* ```git clone https://paulo_aurelio@bitbucket.org/paulo_aurelio/shopping-catalog-cart-api.git```

After clone repository from bitbucket:

* Install the project:
```
cd shopping-catalog-cart-api
npm install
```

* Start the API
```
npm start
```

## Testing

In order to do testing more understandable, I have described below how the api works, by following the logical order of the calling the api`s endpoints.

### Testing AUTHENTICATED Endpoints

For testing the endpoint proteced by JWT Access token, I prefer to use:

* ```curl``` Terminal command


### Testing NO AUTHENTICATED Endpoints

Using a Chrome browser instance, call this url:

* http://localhost:3000/graphql

This url will give us the access to the *GraphiQl* web interface.

## Starting Tests

### Create User (NO AUTHENTICATED - Tested in ```GraphiQL```)

```
mutation{
  createUser(userInput:{email: "paulo@test.com", password:"123"}){
    email
  }
}
```
response
```
{
  "data": {
    "createUser": {
      "_id": "5cd1fc831c2a393826a8a6f8",
      "email": "paulo@test.com"
    }
  }
}
```

### Login User (NO AUTHENTICATED - Tested in ```GraphiQL```)

This calling is very important to be able to take the token that will be mandatory to access authenticated endpoint later on. 

```
query{
  login(email: "paulo@test.com", password: "123"){
    token
    tokenExpiration
  }
}
```

responde
```
{
  "data": {
    "login": {
      "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1Y2QxZmM4MzFjMmEzOTM4MjZhOGE2ZjgiLCJlbWFpbCI6InBhdWxvQHRlc3QuY29tIiwiaWF0IjoxNTU3Mjc5ODYyLCJleHAiOjE1NTcyODM0NjJ9.E4ngIw-xIC-et8C8bE6CaKPFbl0rCycZVUvqyJ8eWfs",
      "tokenExpiration": 1
    }
  }
}
```
Obs1: this is the ```curl``` calling version:
```
curl -i -H 'Content-Type: application/json' -X POST -d '{"query": "query {login(email:\"paulo@test.com\", password:\"123\"){ token tokenExpiration }}"}' http://localhost:3000/graphql

```
Obs2: this user ```token``` has 1 hour of duration, After that, a new one is necessary.



###  Create Products - (AUTHENTICATED - Tested in ```curl``` )

*These callings need to the user ```token``` retreived from ```login``` endpoint*

```Product 1```
```
mutation{
  createProduct(productInput:{
     name:"Sledgehammer", 
     description: "Heavy Hammer", 
     price: 125.76, available:10})
  {
    name
    price
  }
}
```

```
curl -i -H 'Content-Type: application/json' -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1Y2QxZmM4MzFjMmEzOTM4MjZhOGE2ZjgiLCJlbWFpbCI6InBhdWxvQHRlc3QuY29tIiwiaWF0IjoxNTU3MjczMjgyLCJleHAiOjE1NTcyNzY4ODJ9.Wr2dWkjMNx37DKz-2qXhujHljEIoUJ36H78PSRiDMBo" -X POST -d '{"query": "mutation {createProduct(productInput:{ name:\"Sledgehammer\", description: \"Heavy Hammer\", price: 125.76, available:10}){ _id name price}}"}' http://localhost:3000/graphql

HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 98
ETag: W/"62-bUIUxIX9Ft09bYBJqtxg9urVTIc"
Date: Wed, 08 May 2019 00:09:18 GMT
Connection: keep-alive

{"data":{"createProduct":{"_id":"5cd21e2e25811338eda5092a","name":"Sledgehammer","price":125.76}}}
```

```Product 2```
```
mutation{
createProduct(productInput:{
     name:"Axe", 
     description: "Heavy Axe", 
     price: 190.51, available:10})
  {
    name
    price
  } 
}
```

```
curl -i -H 'Content-Type: application/json' -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1Y2QxZmM4MzFjMmEzOTM4MjZhOGE2ZjgiLCJlbWFpbCI6InBhdWxvQHRlc3QuY29tIiwiaWF0IjoxNTU3MjczMjgyLCJleHAiOjE1NTcyNzY4ODJ9.Wr2dWkjMNx37DKz-2qXhujHljEIoUJ36H78PSRiDMBo" -X POST -d '{"query": "mutation {createProduct(productInput:{ name:\"Axe\", description: \"Heavy Axe\", price: 190.51, available:10}){ _id name price}}"}' http://localhost:3000/graphql

HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 89
ETag: W/"59-oVsqlkOrWs2Kh/zlT4BGotwDA0s"
Date: Wed, 08 May 2019 00:14:15 GMT
Connection: keep-alive

{"data":{"createProduct":{"_id":"5cd21f5725811338eda5092b","name":"Axe","price":190.51}}}

```

```Product 3```
```
mutation{
createProduct(productInput:{
     name:"Bandsaw", 
     description: "Heavy Bandsaw", 
     price: 562.14, available:10})
  {
    name
    price
  }
}
```

```
curl -i -H 'Content-Type: application/json' -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1Y2QxZmM4MzFjMmEzOTM4MjZhOGE2ZjgiLCJlbWFpbCI6InBhdWxvQHRlc3QuY29tIiwiaWF0IjoxNTU3MjczMjgyLCJleHAiOjE1NTcyNzY4ODJ9.Wr2dWkjMNx37DKz-2qXhujHljEIoUJ36H78PSRiDMBo" -X POST -d '{"query": "mutation {createProduct(productInput:{ name:\"Bandsaw\", description: \"Heavy Bandsaw\", price: 562.14, available:10}){ _id name price}}"}' http://localhost:3000/graphql

HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 93
ETag: W/"5d-dTynaM45yp8zWrWsQqz/6JWYHLE"
Date: Wed, 08 May 2019 00:15:30 GMT
Connection: keep-alive

{"data":{"createProduct":{"_id":"5cd21fa225811338eda5092c","name":"Bandsaw","price":562.14}}}

```

```Product 4```
```
mutation{
  createProduct(productInput:{
     name:"Chisel", 
     description: "Heavy Chisel", 
     price: 13.9, available:10})
  {
    _id
    name
    price
  }
}
```

```
curl -i -H 'Content-Type: application/json' -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1Y2QxZmM4MzFjMmEzOTM4MjZhOGE2ZjgiLCJlbWFpbCI6InBhdWxvQHRlc3QuY29tIiwiaWF0IjoxNTU3MjczMjgyLCJleHAiOjE1NTcyNzY4ODJ9.Wr2dWkjMNx37DKz-2qXhujHljEIoUJ36H78PSRiDMBo" -X POST -d '{"query": "mutation {createProduct(productInput:{ name:\"Chisel\", description: \"Heavy Chisel\", price: 13.9, available:10}){ _id name price}}"}' http://localhost:3000/graphql

HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 90
ETag: W/"5a-iH0Xlde3CqWMNQ/tnKTWxPy1Yog"
Date: Wed, 08 May 2019 00:17:57 GMT
Connection: keep-alive

{"data":{"createProduct":{"_id":"5cd2203525811338eda5092d","name":"Chisel","price":13.9}}}
```

```Product 5```
```
mutation{
  createProduct(productInput:{
     name:"Hacksaw", 
     description: "Heavy Hacksaw", 
     price: 19.45, available:10})
  {
    name
    price
  }
}
```

```
curl -i -H 'Content-Type: application/json' -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1Y2QxZmM4MzFjMmEzOTM4MjZhOGE2ZjgiLCJlbWFpbCI6InBhdWxvQHRlc3QuY29tIiwiaWF0IjoxNTU3MjczMjgyLCJleHAiOjE1NTcyNzY4ODJ9.Wr2dWkjMNx37DKz-2qXhujHljEIoUJ36H78PSRiDMBo" -X POST -d '{"query": "mutation {createProduct(productInput:{ name:\"Hacksaw\", description: \"Heavy Hacksaw\", price: 19.45, available:10}){ _id name price}}"}' http://localhost:3000/graphql

HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 92
ETag: W/"5c-cjx5VQJqKeIOIGIvMF8aas8dJA0"
Date: Wed, 08 May 2019 00:18:53 GMT
Connection: keep-alive

{"data":{"createProduct":{"_id":"5cd2206d25811338eda5092e","name":"Hacksaw","price":19.45}}}
```

### Product catalog - Retrieve the List of the Produtcts (NO AUTHENTICATED - Tested in ```GraphiQL```)


```
query{
  products{
    name
    price
  }
}
```

```
{
  "data": {
    "products": [
      {
        "name": "Sledgehammer",
        "price": 125.76
      },
      {
        "name": "Axe",
        "price": 190.51
      },
      {
        "name": "Bandsaw",
        "price": 562.14
      },
      {
        "name": "Chisel",
        "price": 13.9
      },
      {
        "name": "Hacksaw",
        "price": 19.45
      }
    ]
  }
}
```

### Create Cart - (AUTHENTICATED - Tested in ```curl``` )

*This calling needs to the user ```token``` retreived from ```login``` endpoint*

```
mutation{
  createCart(cartInput:{totalPrice: 0.0, totalItems: 0}){
       _id
    creator {
      email
    }
    totalPrice
    totalItems
    cartItems {
      _id
      product {
        _id
        name
      }
    }
  }
}
```

```
curl -i -H 'Content-Type: application/json' -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1Y2QxZmM4MzFjMmEzOTM4MjZhOGE2ZjgiLCJlbWFpbCI6InBhdWxvQHRlc3QuY29tIiwiaWF0IjoxNTU3MjgxMzg2LCJleHAiOjE1NTcyODQ5ODZ9.XYBGCrbVhyLcpY0hr1MHujTUqLP69PFWpremHuQOlYM" -X POST -d '{"query": "mutation {createCart(cartInput:{totalPrice: 0.0, totalItems: 0}){ _id creator { email } totalPrice totalItems cartItems{ _id product {_id name }}}}"}' http://localhost:3000/graphql

HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 140
ETag: W/"8c-kA0IEM/ThnQDIi3fZttV4T55i4g"
Date: Wed, 08 May 2019 02:10:43 GMT
Connection: keep-alive

{"data":{"createCart":{"_id":"5cd23aa390050f3ab1e3c180","creator":{"email":"paulo@test.com"},"totalPrice":0,"totalItems":0,"cartItems":[]}}}

```

### Add Product to Cart (AUTHENTICATED - Tested in ```curl``` )

* *These callings need to the user ```token``` retreived from the ```login``` endpoint*
* *```cartId``` retreived from the ```createCart``` endpoint*
* *```productId``` retreived from the ```createProduct``` endpoint*


```Adding Product 5 to Cart```
```
mutation{
  addProductToCart(cartItemInput:{cartId:"5cd23aa390050f3ab1e3c180", productId:"5cd2206d25811338eda5092e"}){
    _id
    product {
      _id
      name
      price
    }
    quantity
    total
  }
}
```

```
curl -i -H 'Content-Type: application/json' -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1Y2QxZmM4MzFjMmEzOTM4MjZhOGE2ZjgiLCJlbWFpbCI6InBhdWxvQHRlc3QuY29tIiwiaWF0IjoxNTU3MjgxMzg2LCJleHAiOjE1NTcyODQ5ODZ9.XYBGCrbVhyLcpY0hr1MHujTUqLP69PFWpremHuQOlYM" -X POST -d '{"query": "mutation { addProductToCart(cartItemInput:{cartId: \"5cd23aa390050f3ab1e3c180\", productId:\"5cd2206d25811338eda5092e\" }){ _id product {_id name price} quantity total}}"}' http://localhost:3000/graphql

HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 167
ETag: W/"a7-Y1Xq2LHTohWtYbVuwp/KdqDqvmQ"
Date: Wed, 08 May 2019 02:33:57 GMT
Connection: keep-alive

{"data":{"addProductToCart":{"_id":"5cd2401590050f3ab1e3c181","product":{"_id":"5cd23aa390050f3ab1e3c180",
"name":"Hacksaw","price":19.45},"quantity":1,"total":19.45}}}
```

```Adding Product 3 to Cart```
```
mutation{
  addProductToCart(cartItemInput:{cartId:"5cd23aa390050f3ab1e3c180", productId:"5cd21fa225811338eda5092c"}){
    _id
    product {
      _id
      name
      price
    }
    quantity
    total
  }
}
```

```
curl -i -H 'Content-Type: application/json' -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1Y2QxZmM4MzFjMmEzOTM4MjZhOGE2ZjgiLCJlbWFpbCI6InBhdWxvQHRlc3QuY29tIiwiaWF0IjoxNTU3MjgxMzg2LCJleHAiOjE1NTcyODQ5ODZ9.XYBGCrbVhyLcpY0hr1MHujTUqLP69PFWpremHuQOlYM" -X POST -d '{"query": "mutation { addProductToCart(cartItemInput:{cartId: \"5cd23aa390050f3ab1e3c180\", productId:\"5cd21fa225811338eda5092c\" }){ _id product {_id name price} quantity total}}"}' http://localhost:3000/graphql

HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 169
ETag: W/"a9-wovdmfJUwnLSDsv4wxsEDo56fg0"
Date: Wed, 08 May 2019 02:37:21 GMT
Connection: keep-alive

{"data":{"addProductToCart":{"_id":"5cd240e190050f3ab1e3c183","product":{"_id":"5cd21fa225811338eda5092c","name":"Bandsaw","price":562.14},"quantity":1,"total":562.14}}}
```

```Adding Product 3 to Cart Again!```

One more time I added this one: (Two Instance of the same product in the cartItem!)
*Note: Keeping the same cartItem/product, istead of create a new one. (just changing the totals)!*

```
curl -i -H 'Content-Type: application/json' -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1Y2QxZmM4MzFjMmEzOTM4MjZhOGE2ZjgiLCJlbWFpbCI6InBhdWxvQHRlc3QuY29tIiwiaWF0IjoxNTU3MjgxMzg2LCJleHAiOjE1NTcyODQ5ODZ9.XYBGCrbVhyLcpY0hr1MHujTUqLP69PFWpremHuQOlYM" -X POST -d '{"query": "mutation { addProductToCart(cartItemInput:{cartId: \"5cd23aa390050f3ab1e3c180\", productId:\"5cd21fa225811338eda5092c\" }){ _id product {_id name price} quantity total}}"}' http://localhost:3000/graphql


HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 170
ETag: W/"aa-xOpJ50ECMWzmyaSzepfOtb1EXmI"
Date: Wed, 08 May 2019 02:42:06 GMT
Connection: keep-alive

{"data":{"addProductToCart":{"_id":"5cd240e190050f3ab1e3c183","product":{"_id":"5cd21fa225811338eda5092c","name":"Bandsaw","price":562.14},"quantity":2,"total":1124.28}}}
```

### Show how the cart was afected by adding CartItems/Products above   (NO AUTHENTICATED - Tested in ```GraphiQL```)

```
query {
  carts {
    _id
    creator {
      email
    }
    totalPrice
    totalItems
    cartItems {
      _id
      product {
        _id
        name
        price
      }
      quantity
      total
    }
  }
}
```

```
{
  "data": {
    "carts": [
      {
        "_id": "5cd23aa390050f3ab1e3c180",
        "creator": {
          "email": "paulo@test.com"
        },
        "totalPrice": 1143.73,
        "totalItems": 3,
        "cartItems": [
          {
            "_id": "5cd2401590050f3ab1e3c181",
            "product": {
              "_id": "5cd2206d25811338eda5092e",
              "name": "Hacksaw",
              "price": 19.45
            },
            "quantity": 1,
            "total": 19.45
          },
          {
            "_id": "5cd240e190050f3ab1e3c183",
            "product": {
              "_id": "5cd21fa225811338eda5092c",
              "name": "Bandsaw",
              "price": 562.14
            },
            "quantity": 2,
            "total": 1124.28
          }
        ]
      }
    ]
  }
}
```

### Remove Product from Cart (AUTHENTICATED - Tested in ```curl``` )

* *This calling needs to the user ```token``` retreived from the ```login``` endpoint*
* *```cartId``` retreived from the ```createCart``` endpoint*
* *```productId``` retreived from the ```createProduct``` endpoint*

```
mutation{
  removeProductFromCart(cartItemInput:{cartId:"5cd23aa390050f3ab1e3c180", productId:"5cd2206d25811338eda5092e"}){
    _id
    name
  }
}
``` 

```
curl -i -H 'Content-Type: application/json' -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1Y2QxZmM4MzFjMmEzOTM4MjZhOGE2ZjgiLCJlbWFpbCI6InBhdWxvQHRlc3QuY29tIiwiaWF0IjoxNTU3MjgxMzg2LCJleHAiOjE1NTcyODQ5ODZ9.XYBGCrbVhyLcpY0hr1MHujTUqLP69PFWpremHuQOlYM" -X POST -d '{"query": "mutation { removeProductFromCart(cartItemInput:{cartId: \"5cd23aa390050f3ab1e3c180\", productId:\"5cd21fa225811338eda5092c\" }){ _id name}}"}' http://localhost:3000/graphql

HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 86
ETag: W/"56-KIw/HhjWWzOgiDNVzCSK3uiiKn8"
Date: Wed, 08 May 2019 02:53:22 GMT
Connection: keep-alive

{"data":{"removeProductFromCart":{"_id":"5cd21fa225811338eda5092c","name":"Bandsaw"}}}
```

### Show how the cart was afected by removing one product instance (NO AUTHENTICATED - Tested in ```GraphiQL```)

```
query {
  carts {
    _id
    creator {
      email
    }
    totalPrice
    totalItems
    cartItems {
      _id
      product {
        _id
        name
        price
      }
      quantity
      total
    }
  }
}
```

```
{
  "data": {
    "carts": [
      {
        "_id": "5cd23aa390050f3ab1e3c180",
        "creator": {
          "email": "paulo@test.com"
        },
        "totalPrice": 581.59,
        "totalItems": 2,
        "cartItems": [
          {
            "_id": "5cd2401590050f3ab1e3c181",
            "product": {
              "_id": "5cd2206d25811338eda5092e",
              "name": "Hacksaw",
              "price": 19.45
            },
            "quantity": 1,
            "total": 19.45
          },
          {
            "_id": "5cd240e190050f3ab1e3c183",
            "product": {
              "_id": "5cd21fa225811338eda5092c",
              "name": "Bandsaw",
              "price": 562.14
            },
            "quantity": 1,
            "total": 562.14
          }
        ]
      }
    ]
  }
}
```

### Remove one more product instance from Cart (AUTHENTICATED - Tested in ```curl``` )

* *This calling needs to the user ```token``` retreived from the ```login``` endpoint*
* *```cartId``` retreived from the ```createCart``` endpoint*
* *```productId``` retreived from the ```createProduct``` endpoint*

```
mutation{
  removeProductFromCart(cartItemInput:{cartId:"5cd23aa390050f3ab1e3c180", productId:"5cd176f73ad73d183cd6a403"}){
    _id
    name
  }
}
```

```
curl -i -H 'Content-Type: application/json' -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1Y2QxZmM4MzFjMmEzOTM4MjZhOGE2ZjgiLCJlbWFpbCI6InBhdWxvQHRlc3QuY29tIiwiaWF0IjoxNTU3MjgxMzg2LCJleHAiOjE1NTcyODQ5ODZ9.XYBGCrbVhyLcpY0hr1MHujTUqLP69PFWpremHuQOlYM" -X POST -d '{"query": "mutation { removeProductFromCart(cartItemInput:{cartId: \"5cd23aa390050f3ab1e3c180\", productId:\"5cd2206d25811338eda5092e\" }){ _id name}}"}' http://localhost:3000/graphql

HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 86
ETag: W/"56-KIw/HhjWWzOgiDNVzCSK3uiiKn8"
Date: Wed, 08 May 2019 02:53:22 GMT
Connection: keep-alive

{"data":{"removeProductFromCart":{"_id":"5cd2206d25811338eda5092e","name":"Hacksaw"}}}
```

### Final view of this test: Shows how the cart was afected by it. - (NO AUTHENTICATED - Tested in ```GraphiQL```)

```
query {
  carts {
    _id
    creator {
      email
    }
    totalPrice
    totalItems
    cartItems {
      _id
      product {
        _id
        name
        price
      }
      quantity
      total
    }
  }
}
```

```
{
  "data": {
    "carts": [
      {
        "_id": "5cd23aa390050f3ab1e3c180",
        "creator": {
          "email": "paulo@test.com"
        },
        "totalPrice": 562.14,
        "totalItems": 1,
        "cartItems": [
          {
            "_id": "5cd240e190050f3ab1e3c183",
            "product": {
              "_id": "5cd21fa225811338eda5092c",
              "name": "Bandsaw",
              "price": 562.14
            },
            "quantity": 1,
            "total": 562.14
          }
        ]
      }
    ]
  }
}
```

## Other queries can be create with GraphQL, not just the scenario above!
