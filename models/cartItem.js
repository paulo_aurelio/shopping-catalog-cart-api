"use strict";

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const cartItemSchema = new Schema(
    {
        cart: {
            type: Schema.Types.ObjectId,
            ref: 'Cart'
        },
        product: {
            type: Schema.Types.ObjectId,
            ref: 'Product'
        }, 
        quantity: {
            type: Number,
            required: true
        },
        total:{
            type: Number,
            required: true
        }        
    },
    { timestamps: true}
)

module.exports = mongoose.model('CartItem', cartItemSchema);
