"use strict";

module.exports = {
    formatDecimals : num => {
        let n = 2;   
        return parseFloat(Math.round(num * Math.pow(10, n)) / Math.pow(10,n)).toFixed(n);
    },
    
    dateToString : date => {
        return new Date(date).toISOString();
    }
};