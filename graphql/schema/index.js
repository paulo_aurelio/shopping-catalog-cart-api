"use strict";

const { buildSchema } = require('graphql');

module.exports = buildSchema(`

type User {
    _id: ID!
    email: String!
    password: String
    createdCarts: [Cart!]
}

input UserInput {
    email: String!
    password: String!
}

type Product {
    _id: ID!
    name: String!
    description: String!
    price: Float!
    available: Int!
} 

input ProductInput {
    name: String!
    description: String!
    price: Float!
    available: Int!
}

type Cart {
    _id: ID!
    creator: User!
    totalPrice: Float!
    totalItems: Int!
    cartItems : [CartItem!]
}

input CartInput {
    totalPrice: Float!
    totalItems: Int! 
}

type CartItem {
    _id: ID!    
    cart: Cart!
    product: Product!
    quantity: Int!
    total: Float!
    createdAt: String!
    updatedAt: String!
}

input CartItemInput {
    cartId: ID!
    productId: ID!

}

type AuthData{
    userId: ID!
    token: String!
    tokenExpiration: Int!
}

type RootQuery {
    products: [Product!]!
    carts: [Cart!]!
    login(email: String!, password: String!): AuthData!
}

type RootMutation {
    createProduct( productInput:ProductInput ): Product
    createUser( userInput: UserInput ): User
    createCart( cartInput: CartInput ): Cart
    addProductToCart( cartItemInput: CartItemInput  ) : CartItem
    removeProductFromCart( cartItemInput: CartItemInput ) : Product 
}

schema {
    query: RootQuery
    mutation: RootMutation
}
`);