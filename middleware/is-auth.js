"use strict";

const jwt = require('jsonwebtoken');


module.exports = (req, res, next) => {
    // taking the auth header from request:
    // Authorization: Bearer <TOKEN>
    const authHeader = req.get('Authorization');
    if (!authHeader){
        req.isAuth = false;
        return next();
    }
    // Spliting auth header up!
    // 0[Bearer] 1[<TOKEN>]
    // 
    const token = authHeader.split(' ')[1];
    if (!token || token === ''){
        req.isAuth = false;
        return next();
    }
    // Decode token received
    let decodedToken;
    try{
        decodedToken = jwt.verify(token, 'lajsdas_mysupersecretkey_haskdahk')
    } catch(err) {
        req.isAuth = false;
        return next();
    }
    // Token was Not Decoded!!!
    if(!decodedToken){
        req.isAuth = false;
        return next();
    }
    // Now we have a valid token
    req.isAuth = true;
    req.userId = decodedToken.userId;
    return next();
}