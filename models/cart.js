"use strict";

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const cartSchema = new Schema(
    {
        creator: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        },
        cartItems:[
            {
                type: Schema.Types.ObjectId,
                ref: 'CartItem'
            }
        ],
        totalItems: {
            type: Number,
            required:true
        },
        totalPrice: {
            type: Number,
            required: true
        },
 
    }
)

module.exports = mongoose.model('Cart', cartSchema);
 