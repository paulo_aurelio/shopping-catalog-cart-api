"use strict";

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const Product = require('../../models/product');
const User = require('../../models/user');
const Cart = require('../../models/cart');
const CartItem = require('../../models/cartItem');

const {formatDecimals, dateToString} = require('../../helpers/index');
 
const getCarts = async cartIds => {
    try{
        const carts = await Cart.find({_id: {$in: cartIds} });
        return carts.map(cart => {
            return {
                ...cart._doc,
                _id: cart.id,                
                creator: getUser.bind(this, cart.creator),
                totalPrice: formatDecimals(cart.totalPrice)
            };
        });
   } catch (err) {
        throw err;
   }
}

const getCartItems = async cartId => {
    try{
        const cartItems = await CartItem.find({ cart: cartId }).populate('product').populate('cart');
        return cartItems.map(item => {
            return {
                ...item._doc,
                _id: item.id,           
                createdAt: dateToString(item._doc.createdAt), 
                updatedAt: dateToString(item._doc.updatedAt),
                total: formatDecimals(item.total),
                product: getProduct.bind(this, item._doc.product)
            };
        });
   } catch (err) {
        throw err;
   }
}

const getUser = async userId => {
    try{
        const user = await User.findById(userId);
        return { 
            ...user._doc, 
            _id: user.id, 
            createdCarts: getCarts.bind(this, user._doc.createdCarts) 
        };
    } catch(err) {
        throw err;
    }
}

const getProduct = async productId => {
    try{
        const product = await Product.findById(productId);
        return { 
            ...product._doc, 
            _id: product.id,
            price: formatDecimals(product.price)
        };
    } catch(err) {
        throw err;
    }
}

module.exports = {
    products : async () => {
        try {
            const products = await Product.find();
            return products.map(product => {    
                return { 
                    ...product._doc,
                    _id: product.id, 
                    price: formatDecimals(product.price)
                };
            });
        } catch(err) {
            console.log(err);
            throw err;
        };
    },
    carts : async () => {
        try {
            const carts = await Cart.find().populate('creator');
            return carts.map(cart => {    
                return { 
                    ...cart._doc,
                    _id: cart.id,
                    creator: getUser.bind(this, cart._doc.creator),
                    cartItems: getCartItems.bind(this, cart.id),
                    totalPrice: formatDecimals(cart.totalPrice)
                };
            });  
        } catch(err) {
            console.log(err);
            throw err;
        };
    },
    login: async ({email, password}) => {
        //
        const loginUser = await User.findOne({email: email});
        //
        if(!loginUser) {
            throw new Error('User does nor exist!');
        }
        const isEqual = await bcrypt.compare(password, loginUser.password);
        if(!isEqual) {
            throw new Error('Password is incorect!');
        }
        //  create JWT Token (syncronous task)
        const token = jwt.sign(
            {userId: loginUser.id, email: loginUser.email},
            'lajsdas_mysupersecretkey_haskdahk',
            {expiresIn: '1h'}
        );

        return {
            userId: loginUser.id,
            token: token,
            tokenExpiration: 1 
        } 

    },
    createUser: async args => {
        try {          
            const user = await User.findOne({email: args.userInput.email});        
            if (user) {
                throw new Error('User exists already.');
            }            
            const hashedPassword = await bcrypt.hash(args.userInput.password, 12);
            const userWithHashedPassword = new User({
                email: args.userInput.email,
                password: hashedPassword
            });  
            const result = await userWithHashedPassword.save();
            return { 
                ...result._doc, 
                password: null, 
                _id: result.id 
            };       
        } catch(err) {
            console.log(err);
            throw err;
        };
    },
    createProduct: async (args, req) => {
        // Protected Endpoint
        if(!req.isAuth){
            throw new Error('Unauthenticated access!');
        }        
        try {
            const product = new Product({
                name: args.productInput.name,
                description: args.productInput.description,
                price: +args.productInput.price,
                available: args.productInput.available   
            });
            //
            const result = await product.save();
            console.log(result)
            return { 
                ...result._doc,
                 _id: result._doc._id.toString(),
                 price: formatDecimals(result.price) 
            };                    
        } catch(err) {
            console.log(err);
            throw err;
        };
    },
    createCart: async (args, req) => {
        // Protected Endpoint
        if(!req.isAuth){
            throw new Error('Unauthenticated access!');
        }
        try {
            //Fetch User that will own the Cart
            const creatorUser = await User.findById(req.userId);
            if (!creatorUser) {
                throw new Error('User does not exist.');
            }

            const cart = new Cart({
                totalPrice: +args.cartInput.totalPrice,
                totalItems: +args.cartInput.totalItems,
                creator: creatorUser._doc._id
            });
            //Save cart
            const result = await cart.save();                                
            
            //Adding new cart to user's carts
            creatorUser.createdCarts.push(result._doc);
            
            // Update User with new Cart!
            await creatorUser.save();    

            // return the Created Cart               
            return { 
                ...result._doc, 
                _id: result.id,
                creator: getUser.bind(this, result._doc.creator),
                totalPrice: formatDecimals(result.totalPrice)
            };
        } catch(err){
            console.log(err);
            throw err;
        };
    },
    addProductToCart: async (args,req) => {
        // Protected Endpoint
        if(!req.isAuth){
            throw new Error('Unauthenticated access!');
        }        
        try{
            //
            const product = await Product.findOne({_id: args.cartItemInput.productId})
            if (!product) {
                throw new Error('Product does not exist.');
            }
            //
            const cart = await Cart.findOne({_id: args.cartItemInput.cartId})
            if (!cart) {
                throw new Error('Cart does not exist.');
            }
            // Verify if the fetched Cart already has the fetched product 
            const cartItem = await CartItem.findOne({
                cart: cart._doc._id,
                product: product._doc._id
            });

            let savedCartItem; 
            if (cartItem) {  
                //using the same cartItem, if found!              
                cartItem.quantity += 1;
                cartItem.total = cartItem.quantity * product.price ; 
                savedCartItem = await cartItem.save();
            } else {
                // Create New CartItem.
                const newItem = new CartItem({
                    cart: cart._doc._id,
                    product: product._doc._id,
                    quantity: 1,
                    total: product._doc.price
                })
                savedCartItem = await newItem.save();
                cart.cartItems.push(savedCartItem);
            };
            // Update overall total of the Cart
            cart.totalPrice += product.price;
            cart.totalItems += 1;
            await cart.save();
            // return CartItem (updated or created)
            return {
                ...savedCartItem._doc,
                _id: savedCartItem.id, 
                product: getProduct.bind(this, savedCartItem._doc.product), 
                total: formatDecimals(savedCartItem.total),             
                createdAt: new Date(savedCartItem._doc.createdAt).toISOString(), 
                updatedAt: new Date(savedCartItem._doc.updatedAt).toISOString()
            }; 
        } catch(err) {
            console.log(err);
            throw err;
        }
    },
    removeProductFromCart: async (args, req) => {
        // Protected Endpoint
        if(!req.isAuth){
            throw new Error('Unauthenticated access!');
        }        
        try{
            //
            const product = await Product.findOne({_id: args.cartItemInput.productId})
            if (!product) {
                throw new Error('Product does not exist.');
            }
            //
            const cart = await Cart.findOne({_id: args.cartItemInput.cartId})
            if (!cart) {
                throw new Error('Cart does not exist.');
            }
            // Verify if the fetched Cart already has the fetched product 
            const cartItem = await CartItem.findOne({
                cart: cart._doc._id,
                product: product._doc._id
            });

            if (!cartItem) {
                throw new Error('CartItem does not exist.');
            }

            // If CartItem has more than 1 instances of the product.
            if (cartItem.quantity > 1) {                
                cartItem.quantity -= 1;
                cartItem.total -= product.price ; 
                await cartItem.save();
            } else {
                // Delete cartItem.
                await CartItem.deleteOne( {_id: cartItem._id})
                // Delete from relations with Cart.
                cart.cartItems = cart.cartItems.filter(item => {                                      
                    return (item.toString() != cartItem._id.toString());
                });                
            };
            // Update overall total of the Cart
            cart.totalPrice -= product.price;
            cart.totalItems -= 1;
            await cart.save();
            // return CartItem (updated or created)
            return {
                ...product._doc,
                _id: product.id,
                price: formatDecimals(product.price)
            };            
        } catch(err) {
            console.log(err);
            throw err;
        }
    }
};